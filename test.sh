#!/bin/bash

set -e

for folder in */; do
    rm -rf "$folder"build
    mkdir "$folder"build
    cd "$folder"build
    cmake ..
    make
    cd ../../
done
