/******************************************************************************
 * Author: Javier Reyes
 * Date:   19.09.2020
 *****************************************************************************/

/**************************** System headers *********************************/
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <pthread.h>
#include <semaphore.h>

/****************************** User headers *********************************/
#include "main.h"

/**************************** Defines - Macros *******************************/
#define BUFFER_SIZE     100
#define ITER            100

/********************** Variables / Data Structures **************************/
int buffer[BUFFER_SIZE];
int pindex = 0;
int cindex = 0;
sem_t spaces;
sem_t items;
pthread_mutex_t mutex;
unsigned int seed = 252;

/*************************** Function Prototypes *****************************/


/************************* Function Implementation ***************************/
int produce(int id)
{
    int r = rand_r(&seed);
    printf("Producer %d produced %d.\n", id, r);
    return r;
}

void consume(int id, int number)
{
    printf("Consumer %d consumed %d.\n", id, number);
}

void* producer(void* arg)
{
    int* id = (int*)arg;
    for(int i = 0; i < ITER; i++)
    {
        int num = produce(*id);
        sem_wait(&spaces);
        pthread_mutex_lock(&mutex);
        buffer[pindex] = num;
        pindex = (pindex + 1) % BUFFER_SIZE;
        pthread_mutex_unlock(&mutex);
        sem_post(&items);
    }
    free(arg);
    pthread_exit(NULL);
}

void* consumer(void* arg)
{
    int* id = (int*)arg;
    for(int i = 0; i < ITER; i++)
    {
        sem_wait(&items);
        pthread_mutex_lock(&mutex);
        int num = buffer[cindex];
        buffer[cindex] = -1;
        cindex = (cindex + 1) % BUFFER_SIZE;
        pthread_mutex_unlock(&mutex);
        sem_post(&spaces);
        consume(*id, num);
    }
    free(id);
    pthread_exit(NULL);
}

/***************************** Entry point ***********************************/
/** \brief main
 */
int main(int argc, char** argv)
{
    sem_init(&spaces, 0, BUFFER_SIZE);
    sem_init(&items, 0, 0);
    pthread_mutex_init(&mutex, NULL);

    pthread_t threads[20];

    for(int i = 0; i < 10; i++)
    {
        int* id = malloc(sizeof(int));
        *id = i;
        pthread_create(&threads[i], NULL, producer, id);
    }
    for(int j = 10; j < 20; j++)
    {
        int* jd = malloc(sizeof(int));
        *jd = j - 10;
        pthread_create(&threads[j], NULL, consumer, jd);
    }
    for(int k = 0; k < 20; k++)
    {
        pthread_join(threads[k], NULL);
    }

    sem_destroy(&spaces);
    sem_destroy(&items);
    pthread_mutex_destroy(&mutex);
    pthread_exit(0);
}
