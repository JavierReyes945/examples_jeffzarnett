/******************************************************************************
 * Author: Javier Reyes
 * Date:   29.07.2020
 *****************************************************************************/

/**************************** System headers *********************************/
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>

/****************************** User headers *********************************/
#include "main.h"

/**************************** Defines - Macros *******************************/


/********************** Variables / Data Structures **************************/
volatile int quit = 0;

/*************************** Function Prototypes *****************************/


/************************* Function Implementation ***************************/
void handle_it(int signal_num)
{
    quit = 1;
}

/***************************** Entry point ***********************************/
/** \brief main
 */
int main(int argc, char** argv)
{
    signal(SIGINT, handle_it);
    while(quit == 0)
    {
        printf("Waiting...\n");
        sleep(1);
    }

    printf("\nTime to die.\n");

    // End of the program
    exit(EXIT_SUCCESS);
}

