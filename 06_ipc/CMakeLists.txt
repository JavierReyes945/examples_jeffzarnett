###############################################################################
# Author: Javier Reyes
# Date:   29.07.2020
###############################################################################

cmake_minimum_required (VERSION 3.16.3)

project (ipc VERSION "0.1")

set (CMAKE_C_STANDARD 11)

include_directories (inc)

add_executable (ipc
    src/main.c
)

target_compile_options(ipc PRIVATE -g -Wall)
