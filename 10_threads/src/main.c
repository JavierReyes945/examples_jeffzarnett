/******************************************************************************
 * Author: Javier Reyes
 * Date:   19.09.2020
 *****************************************************************************/

/**************************** System headers *********************************/
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

/****************************** User headers *********************************/
#include "main.h"

/**************************** Defines - Macros *******************************/


/********************** Variables / Data Structures **************************/


/*************************** Function Prototypes *****************************/


/************************* Function Implementation ***************************/
void* run(void* argument)
{
    char* arg = (char*)argument;
    printf("Provided argument is %s!\n", arg);
    int* return_val = malloc(sizeof(int));
    *return_val = 99;
    pthread_exit(return_val);
}

/***************************** Entry point ***********************************/
/** \brief main
 */
int main(int argc, char** argv)
{
    if(argc != 2)
    {
        printf("Invalid args.\n");
        exit(EXIT_FAILURE);
    }

    pthread_t t;
    void* vr;

    pthread_create(&t, NULL, run, argv[1]);
    pthread_join(t, &vr);
    int* r = (int*)vr;
    printf("The other thread returned %d.\n", *r);
    free(vr);
    pthread_exit(0);
}
