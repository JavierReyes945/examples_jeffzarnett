/******************************************************************************
 * Author: Javier Reyes
 * Date:   29.07.2020
 *****************************************************************************/

/**************************** System headers *********************************/
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>

/****************************** User headers *********************************/
#include "main.h"

/**************************** Defines - Macros *******************************/


/********************** Variables / Data Structures **************************/


/*************************** Function Prototypes *****************************/
void readfile(int fd);

/************************* Function Implementation ***************************/
void readfile(int fd)
{
    int buf_size = 256;
    char* buffer = malloc(buf_size);
    while(1)
    {
        memset(buffer, 0, buf_size);
        int bytes_read = read(fd, buffer, buf_size - 1);
        if(bytes_read == 0)
        {
            break;
        }
        printf( "%s", buffer );
    }
    printf("\nEnd of File.\n");
    free(buffer);
}

/***************************** Entry point ***********************************/
/** \brief main
 */
int main(int argc, char** argv)
{
    if(argc != 2)
    {
        printf("Usage: %s <filename>\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    int fd = open(argv[1], O_RDONLY);
    if(fd == -1)
    {
        printf("Unable to open file! %s is invalid name.\n", argv[1]);
        exit(EXIT_FAILURE);
    }
    readfile(fd);
    close(fd);

    // End of the program
    exit(EXIT_SUCCESS);
}

