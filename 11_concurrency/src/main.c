/******************************************************************************
 * Author: Javier Reyes
 * Date:   19.09.2020
 *****************************************************************************/

/**************************** System headers *********************************/
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

/****************************** User headers *********************************/
#include "main.h"

/**************************** Defines - Macros *******************************/


/********************** Variables / Data Structures **************************/
int sum; // Shared data

/*************************** Function Prototypes *****************************/
void* runner(void* param);

/************************* Function Implementation ***************************/
void* runner(void* param)
{
    int upper = atoi(param);
    sum = 0;
    for(int i = 1; i <= upper; i++)
    {
        sum += i;
    }
    printf("A thread has finished.\n");
    return 0;
}

/***************************** Entry point ***********************************/
/** \brief main
 */
int main(int argc, char** argv)
{
    pthread_t tid[3];

    if(argc != 2)
    {
        fprintf(stderr, "Usage: %s <integer value>\n", argv[0]);
        exit(EXIT_FAILURE);
    }
    if(atoi(argv[1]) < 0)
    {
        fprintf(stderr, "%d must be >= 0\n", atoi(argv[1]));
        exit(EXIT_FAILURE);
    }

    for(int i = 0; i < 3; i++)
    {
        pthread_create(&tid[i], NULL, runner, argv[1]);
    }
    for(int i = 0; i < 3; i++)
    {
        pthread_join(tid[i], NULL);
    }
    printf("sum = %d\n", sum);
    pthread_exit(NULL);
}
