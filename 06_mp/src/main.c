/******************************************************************************
 * Author: Javier Reyes
 * Date:   29.07.2020
 *****************************************************************************/

/**************************** System headers *********************************/
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/msg.h>

/****************************** User headers *********************************/
#include "main.h"

/**************************** Defines - Macros *******************************/


/********************** Variables / Data Structures **************************/
struct msg
{
    long mtype;
    int data;
    double other_data;
};

/*************************** Function Prototypes *****************************/


/************************* Function Implementation ***************************/


/***************************** Entry point ***********************************/
/** \brief main
 */
int main(int argc, char** argv)
{
    int msgqid = msgget(IPC_PRIVATE, 0666 | IPC_CREAT);

    int pid = fork();
    if(pid > 0)
    {
        struct msg m;
        m.mtype = 42;
        m.data = 252;
        m.other_data = 3.14;
        msgsnd(msgqid, &m, sizeof(struct msg), 0);
    }
    else if(pid == 0)
    {
        struct msg m2;
        msgrcv(msgqid, &m2, sizeof(struct msg), 42, 0);
        printf("Received %d, %f!\n", m2.data, m2.other_data);
        msgctl(msgqid, IPC_RMID, NULL);
    }

    // End of the program
    exit(EXIT_SUCCESS);
}

