/******************************************************************************
 * Author: Javier Reyes
 * Date:   29.07.2020
 *****************************************************************************/

/**************************** System headers *********************************/
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netdb.h>

/****************************** User headers *********************************/
#include "main.h"

/**************************** Defines - Macros *******************************/


/********************** Variables / Data Structures **************************/


/*************************** Function Prototypes *****************************/


/************************* Function Implementation ***************************/


/***************************** Entry point ***********************************/
/** \brief main
 */
int main(int argc, char** argv)
{
    int pid = fork();

    if(pid < 0)
    {
        printf("Fork failed.\n");
        exit(EXIT_FAILURE);
    }
    else if(pid == 0)
    {
        printf("Child process sleeping to give server time to set up.\n");
        sleep(4);
        struct addrinfo hints;
        struct addrinfo *serverinfo;
        memset(&hints, 0, sizeof(hints));

        hints.ai_family = AF_INET;
        hints.ai_socktype = SOCK_STREAM;
        hints.ai_flags = AI_PASSIVE;

        int result = getaddrinfo("127.0.0.1", "2520", &hints, &serverinfo);
        struct sockaddr_in* sain = (struct sockaddr_in*) serverinfo->ai_addr;
        int clientsock = socket(serverinfo->ai_family, 
                serverinfo->ai_socktype, serverinfo->ai_protocol);
        connect(clientsock, serverinfo->ai_addr, serverinfo->ai_addrlen);

        char* message = malloc(20);
        memset(message, 0, 20);
        sprintf(message, "Message %d.", 1138);
        send(clientsock, message, strlen(message) + 1, 0);
        close(clientsock);
    }
    else
    {
        struct sockaddr_in addr;
        addr.sin_family = AF_INET;
        addr.sin_port = htons(2520);
        addr.sin_addr.s_addr = htonl(INADDR_ANY);

        int socketfd = socket(AF_INET, SOCK_STREAM, 0);
        bind(socketfd, (struct sockaddr*) &addr, sizeof(addr));
        listen(socketfd, 10);

        int newsockfd;
        char* received = malloc(20);
        newsockfd = accept(socketfd, NULL, NULL);
        memset(received, 0, 20);
        recv(newsockfd, received, 20, 0);
        printf("Server received: %s\n", received);
        close(newsockfd);
        free(received);
        close(socketfd);
    }

    // End of the program
    exit(EXIT_SUCCESS);
}

