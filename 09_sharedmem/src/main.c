/******************************************************************************
 * Author: Javier Reyes
 * Date:   19.09.2020
 *****************************************************************************/

/**************************** System headers *********************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/shm.h>
#include <sys/wait.h>

/****************************** User headers *********************************/


/**************************** Defines - Macros *******************************/


/********************** Variables / Data Structures **************************/


/*************************** Function Prototypes *****************************/


/************************* Function Implementation ***************************/


/***************************** Entry point ***********************************/
/** \brief main
 */
int main(int argc, char** argv)
{
    int shmid = shmget(IPC_PRIVATE, 32, IPC_CREAT | 0666);

    int pid = fork();
    if(pid > 0)
    {
        waitpid(pid, NULL, 0);
        void* mem = shmat(shmid, NULL, 0);
        printf("The msg received from the child is %s.\n", (char*)mem);
        shmdt(mem);
        shmctl(shmid, IPC_RMID, NULL);
    }
    else if(pid == 0)
    {
        void* mem = shmat(shmid, NULL, 0);
        memset(mem, 0, 32);
        sprintf(mem, "Hello World");
        shmdt(mem);
    }

    // End of the program
    exit(EXIT_SUCCESS);
}

