/******************************************************************************
 * Author: Javier Reyes
 * Date:   19.09.2020
 *****************************************************************************/

/**************************** System headers *********************************/
#include <stdlib.h>
#include <pthread.h>

/****************************** User headers *********************************/
#include "main.h"

/**************************** Defines - Macros *******************************/


/********************** Variables / Data Structures **************************/
int global;

/*************************** Function Prototypes *****************************/
void* threadA(void* ignore);
void* threadB(void* ignore);
pthread_mutex_t mutexA;
pthread_mutex_t mutexB;

/************************* Function Implementation ***************************/
void* threadA(void* ignore)
{
    global = 1;
    pthread_mutex_lock(&mutexA);
    pthread_mutex_lock(&mutexB);
    pthread_mutex_unlock(&mutexA);
    pthread_mutex_unlock(&mutexB);
    pthread_exit(0);
}

void* threadB(void* ignore)
{
    global = 2;
    pthread_mutex_lock(&mutexB);
    pthread_mutex_lock(&mutexA);
    pthread_exit(0);
}

/***************************** Entry point ***********************************/
/** \brief main
 */
int main(int argc, char** argv)
{
    pthread_mutex_init(&mutexA, NULL);
    pthread_mutex_init(&mutexB, NULL);

    pthread_t ta;
    pthread_t tb;

    pthread_create(&ta, NULL, threadA, NULL);
    pthread_create(&tb, NULL, threadB, NULL);

    pthread_join(ta, NULL);
    pthread_join(tb, NULL);

    pthread_mutex_destroy(&mutexA);
    pthread_mutex_destroy(&mutexA); //TODO

    pthread_exit(0);
}
