/******************************************************************************
 * Author: Javier Reyes
 * Date:   19.09.2020
 *****************************************************************************/

/**************************** System headers *********************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/shm.h>
#include <sys/wait.h>
#include <sys/mman.h>

/****************************** User headers *********************************/


/**************************** Defines - Macros *******************************/


/********************** Variables / Data Structures **************************/


/*************************** Function Prototypes *****************************/


/************************* Function Implementation ***************************/


/***************************** Entry point ***********************************/
/** \brief main
 */
int main(int argc, char** argv)
{
    int fd = open("example.txt", O_RDWR);

    struct stat st;
    stat("example.txt", &st);
    ssize_t size = st.st_size;
    void* mapped = mmap(NULL, size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);

    int pid = fork();
    if(pid > 0)
    {
        waitpid(pid, NULL, 0);
        printf("The new content of the file is: %s.\n", (char*)mapped);
        munmap(mapped, size);
    }
    else if(pid == 0)
    {
        memset(mapped, 0, size);
        sprintf(mapped, "It is now overwritten");
        msync(mapped, size, MS_SYNC);
        munmap(mapped, size);
    }

    // End of the program
    exit(EXIT_SUCCESS);
}

