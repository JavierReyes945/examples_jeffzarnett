/******************************************************************************
 * Author: Javier Reyes
 * Date:   19.09.2020
 *****************************************************************************/

/**************************** System headers *********************************/
#include <stdio.h>
#include <stdlib.h>
#include <curl/curl.h>

/****************************** User headers *********************************/
#include "main.h"

/**************************** Defines - Macros *******************************/


/********************** Variables / Data Structures **************************/
const char* outputfile = "webpage.out";

/*************************** Function Prototypes *****************************/


/************************* Function Implementation ***************************/
size_t cbfunction(void* ptr, size_t size, size_t nmemb, void* arg)
{
    size_t written = fwrite(ptr, size, nmemb, (FILE*) arg);
    return written;
}

/***************************** Entry point ***********************************/
/** \brief main
 */
int main(int argc, char** argv)
{
    if(argc != 2)
    {
        printf("Usage: %s <url>\n", argv[0]);
        exit(EXIT_FAILURE);
    }
    CURL *handle;
    CURLcode res;
    FILE* f = fopen(outputfile, "w+");

    curl_global_init(CURL_GLOBAL_DEFAULT);

    handle = curl_easy_init();
    if(handle != NULL)
    {
        curl_easy_setopt(handle, CURLOPT_URL, argv[1]);
        curl_easy_setopt(handle, CURLOPT_WRITEFUNCTION, cbfunction);
        curl_easy_setopt(handle, CURLOPT_WRITEDATA, f);
        res = curl_easy_perform(handle);
        if(res != CURLE_OK)
        {
            fprintf(stderr, "curl_easy_perform() failed: %s\n",
                curl_easy_strerror(res));
        }
        curl_easy_cleanup(handle);
    }
    curl_global_cleanup();
    fclose(f);

    // End of the program
    exit(EXIT_SUCCESS);
}

