/******************************************************************************
 * Author: Javier Reyes
 * Date:   19.09.2020
 *****************************************************************************/

/**************************** System headers *********************************/
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <semaphore.h>

/****************************** User headers *********************************/
#include "main.h"

/**************************** Defines - Macros *******************************/
#define BUFFER_SIZE     20
#define ITER            100

/********************** Variables / Data Structures **************************/
sem_t spaces;
sem_t items;
int counter = 0;
int* buffer;

/*************************** Function Prototypes *****************************/


/************************* Function Implementation ***************************/
int produce()
{
    ++counter;
    printf("Produced %d.\n", counter);
    return counter;
}

void consume(int value)
{
    printf("Consumed %d.\n", value);
}

void* producer(void* arg)
{
    int pindex = 0;
    while(counter < ITER)
    {
        int v = produce();
        sem_wait(&spaces);
        buffer[pindex] = v;
        pindex = (pindex + 1) % BUFFER_SIZE;
        sem_post(&items);
    }
    pthread_exit(NULL);
}

void* consumer(void* arg)
{
    int cindex = 0;
    int ctotal = 0;
    while(ctotal < ITER)
    {
        sem_wait(&items);
        int temp = buffer[cindex];
        buffer[cindex] = -1;
        cindex = (cindex + 1) % BUFFER_SIZE;
        sem_post(&spaces);
        consume(temp);
        ++ctotal;
    }
    pthread_exit(NULL);
}

/***************************** Entry point ***********************************/
/** \brief main
 */
int main(int argc, char** argv)
{
    buffer = malloc(BUFFER_SIZE * sizeof(int));
    for(int i = 0; i < BUFFER_SIZE; i++)
    {
        buffer[i] = -1;
    }
    sem_init(&spaces, 0, BUFFER_SIZE);
    sem_init(&items, 0, 0);

    pthread_t prod;
    pthread_t con;

    pthread_create(&prod, NULL, producer, NULL);
    pthread_create(&con, NULL, consumer, NULL);
    pthread_join(prod, NULL);
    pthread_join(con, NULL);

    free(buffer);
    sem_destroy(&spaces);
    sem_destroy(&items);
    pthread_exit(0);
}
