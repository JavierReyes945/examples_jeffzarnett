/******************************************************************************
 * Author: Javier Reyes
 * Date:   19.09.2020
 *****************************************************************************/

/**************************** System headers *********************************/
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include <sys/inotify.h>

/****************************** User headers *********************************/
#include "main.h"

/**************************** Defines - Macros *******************************/


/********************** Variables / Data Structures **************************/
const char filename[] = "file.lock";

/*************************** Function Prototypes *****************************/


/************************* Function Implementation ***************************/


/***************************** Entry point ***********************************/
/** \brief main
 */
int main(int argc, char** argv)
{
    int lockFD;
    bool out_turn = false;

    while(!out_turn)
    {
        lockFD = open(filename, O_CREAT | O_EXCL);
        if(lockFD == -1)
        {
            printf("The lock file exists and process %d will wait its turn...\n",
                getpid());
            int notifyFD = inotify_init();
            /** Read the file descriptor for the notify - we get blocked here
             *  until there's an event that we want */
            uint32_t watched = inotify_add_watch(notifyFD, filename,
                IN_DELETE_SELF);
            int buffer_size = sizeof(struct inotify_event) + strlen(filename) + 1;
            char* event_buffer = malloc(buffer_size);
            printf("Setup complete, waiting for event...\n");
            read(notifyFD, event_buffer, buffer_size);

            struct inotify_event* event = (struct inotify_event*)event_buffer;

            /** Here we can look and see what arrived and decide what to do.
             *  In this example, we're only watching one file and one type of
             *  event, so we don't need to make any decisions now */
            printf("Event ocurred!\n");

            free(event_buffer);
            inotify_rm_watch(lockFD, watched);
            close(notifyFD);
        }
        else
        {
            char* pid = malloc(32);
            memset(pid, 0, 32);
            int bytes_of_pid = sprintf(pid, "%d", getpid());

            write(lockFD, pid, bytes_of_pid);
            free(pid);
            close(lockFD);
            out_turn = true;
        }
    }

    printf("Process %d is in the area protected by file lock.\n", getpid());
    remove(filename);
    exit(EXIT_SUCCESS);
}
