###############################################################################
# Author: Javier Reyes
# Date:   29.07.2020
###############################################################################

cmake_minimum_required (VERSION 3.16.3)

project (filesys VERSION "0.1")

set (CMAKE_C_STANDARD 11)

include_directories (inc)

add_executable (notif
    src/main.c
)

target_compile_options(notif PRIVATE -g -Wall)

target_link_libraries(notif pthread)
