/******************************************************************************
 * Author: Javier Reyes
 * Date:   29.07.2020
 *****************************************************************************/

/**************************** System headers *********************************/
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>

/****************************** User headers *********************************/


/**************************** Defines - Macros *******************************/
#define BUFFER_SIZE     10  //!< Max size for the data buffer to be piped
#define READ_END        0   //!< Position in the pipe for the output
#define WRITE_END       1   //!< Position in the pipe for the input

/********************** Variables / Data Structures **************************/


/*************************** Function Prototypes *****************************/


/************************* Function Implementation ***************************/


/***************************** Entry point ***********************************/
/** \brief main
 */
int main(int argc, char** argv)
{
    char write_msg[BUFFER_SIZE] = "Greetings";
    char read_msg[BUFFER_SIZE];
    int fd[2];
    pid_t pid;

    if(pipe(fd) == -1)
    {
        printf("Pipe failed.\n");
        exit(EXIT_FAILURE);
    }
    printf("The file descriptors are %d and %d.\n", fd[0], fd[1]);

    pid = fork();
    if(pid < 0)
    {
        printf("Fork failed.\n");
        exit(EXIT_FAILURE);
    }
    else if(pid > 0)
    {
        // Parent process
        close(fd[READ_END]);
        write(fd[WRITE_END], write_msg, strlen(write_msg) + 1);
        close(fd[WRITE_END]);
    }
    else
    {
        close(fd[WRITE_END]);
        read(fd[READ_END], read_msg, BUFFER_SIZE);
        printf("Read %s.\n", read_msg);
        close(fd[READ_END]);
    }

    // End of the program
    exit(EXIT_SUCCESS);
}

