/******************************************************************************
 * Author: Javier Reyes
 * Date:   19.09.2020
 *****************************************************************************/

/**************************** System headers *********************************/
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <curl/multi.h>


/****************************** User headers *********************************/
#include "main.h"

/**************************** Defines - Macros *******************************/
#define MAX_WAIT_MSECS  30*1000
#define CNT             4

/********************** Variables / Data Structures **************************/
const char* urls[] =
{
    "https://www.microsoft.com/de-de",
    "https://www.yahoo.com",
    "https://www.wikipedia.org",
    "https://slashdot.org"
};

/*************************** Function Prototypes *****************************/


/************************* Function Implementation ***************************/
size_t cb(char* d, size_t n, size_t l, void* p)
{
    // take care of data here, ignored in this example
    return n * l;
}

void init(CURLM* cm, int i)
{
    CURL* eh = curl_easy_init();
    curl_easy_setopt(eh, CURLOPT_WRITEFUNCTION, cb);
    curl_easy_setopt(eh, CURLOPT_HEADER, 0L);
    curl_easy_setopt(eh, CURLOPT_URL, urls[i]);
    curl_easy_setopt(eh, CURLOPT_PRIVATE, urls[i]);
    curl_easy_setopt(eh, CURLOPT_VERBOSE, 0L);
    curl_multi_add_handle(cm, eh);
}

/***************************** Entry point ***********************************/
/** \brief main
 */
int main(int argc, char** argv)
{
    CURLM* cm = NULL;
    CURL* eh = NULL;
    CURLMsg* msg = NULL;
    CURLcode return_code = 0;
    int still_running = 0;
    int msgs_left = 0;
    int http_status_code;
    const char* szUrl;

    curl_global_init(CURL_GLOBAL_ALL);
    cm = curl_multi_init();

    for(int i = 0; i < CNT; i++)
    {
        init(cm, i);
    }

    curl_multi_perform(cm, &still_running);

    do
    {
        int numfds = 0;
        int res = curl_multi_wait(cm, NULL, 0, MAX_WAIT_MSECS, &numfds);
        if(res != CURLM_OK)
        {
            fprintf(stderr, "Error: curl_muti_wait() returned %d\n", res);
            exit(EXIT_FAILURE);
        }
        curl_multi_perform(cm, &still_running);
    }
    while(still_running);

    while((msg = curl_multi_info_read(cm, &msgs_left)))
    {
        if(msg->msg == CURLMSG_DONE)
        {
            eh = msg->easy_handle;
            return_code = msg->data.result;
            if(return_code != CURLE_OK)
            {
                fprintf(stderr, "CURL error code: %d\n", msg->data.result);
                curl_multi_remove_handle(cm, eh);
                curl_easy_cleanup(eh);
                continue;
            }

            // get HTTP status code
            http_status_code = 0;
            szUrl = NULL;
            curl_easy_getinfo(eh, CURLINFO_RESPONSE_CODE, &http_status_code);
            curl_easy_getinfo(eh, CURLINFO_PRIVATE, &szUrl);

            if(http_status_code == 200)
            {
                printf("200 OK for %s\n", szUrl);
            }
            else
            {
                fprintf(stderr, "GET of %s returned http status code %d\n",
                    szUrl, http_status_code);
            }

            curl_multi_remove_handle(cm, eh);
            curl_easy_cleanup(eh);
        }
        else
        {
            fprintf(stderr, "Error: after curl_multi_info_read(), CURLMsg=%d\n",
                msg->msg);
        }
    }
    curl_multi_cleanup(cm);
    curl_global_cleanup();
    exit(EXIT_SUCCESS);
}
