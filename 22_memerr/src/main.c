/******************************************************************************
 * Author: Javier Reyes
 * Date:   19.09.2020
 *****************************************************************************/

/**************************** System headers *********************************/
#include <stdlib.h>

/****************************** User headers *********************************/
#include "main.h"

/**************************** Defines - Macros *******************************/


/********************** Variables / Data Structures **************************/
typedef struct node
{
    int val;
    struct node* next;
} node_t;

/*************************** Function Prototypes *****************************/


/************************* Function Implementation ***************************/
node_t* insert(node_t* head, int val)
{
    node_t* node = malloc(sizeof(node_t));
    node->val = val;
    node->next = NULL;

    if(head == NULL)
    {
        return node;
    }

    node_t* curr = head;
    while(curr->next != NULL)
    {
        curr = curr->next;
    }
    curr->next = node;
    return head;
}

int remove_node(node_t* head)
{
    if(head->next == NULL)
    {
        int temp = head->val;
        free(head);
        return temp;
    }

    node_t* curr = head;
    while(curr->next->next != NULL)
    {
        curr = curr->next;
    }
    int temp = curr->next->val;
    free(curr->next);
    curr->next = NULL;
    return temp;
}

/***************************** Entry point ***********************************/
/** \brief main
 */
int main(int argc, char** argv)
{
    int n = atoi(argv[1]);
    node_t* list = NULL;
    for(int i = 0; i < n; i++)
    {
        list = insert(list, i);
    }
    for(int i = 0; i < n; i++)
    {
        remove_node(list);
    }
    //remove_node(list);

    int* a = malloc(n * sizeof(int));
    int i = 0;
    while(i++ < n)
    {
        a[i] = i;
    }
    //free(a);
}
