/******************************************************************************
 * Author: Javier Reyes
 * Date:   29.07.2020
 *****************************************************************************/

/**************************** System headers *********************************/
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>

/****************************** User headers *********************************/
#include "main.h"

/**************************** Defines - Macros *******************************/


/********************** Variables / Data Structures **************************/


/*************************** Function Prototypes *****************************/


/************************* Function Implementation ***************************/


/***************************** Entry point ***********************************/
/** \brief main
 */
int main(int argc, char** argv)
{
    pid_t pid;
    int child_status;

    pid = fork();
    if(pid < 0)
    {
        printf("Error ocurred: %d.\n", pid);
        exit(EXIT_FAILURE);
    }
    else if(pid == 0)
    {
        printf("Child executing...\n");
    }
    else
    {
        printf("Parent executing...\n");
        // Parent process
        waitpid(pid, &child_status, 0);
        printf("Child returned %d.\n", child_status);
    }

    // End of the program
    exit(EXIT_SUCCESS);
}

